import {useState,useEffect, useContext} from 'react';
import Button from 'react-bootstrap/Button'
import Card from 'react-bootstrap/Card'
import PropTypes from 'prop-types'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'
import AppHelper from '../app_helper'



export default function Category({categoryProp}){

	const {user} = useContext(UserContext)

	const {categoryName, categoryType} = categoryProp
	
	return(
		<tr>
			<td>{categoryName}</td>
			<td>{categoryType}</td>
		</tr>
	)
}

