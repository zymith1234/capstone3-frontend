import {useState,useEffect, useContext} from 'react';
import Button from 'react-bootstrap/Button'
import Card from 'react-bootstrap/Card'
import PropTypes from 'prop-types'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'
import AppHelper from '../app_helper'
import {Row,Col} from 'react-bootstrap'
import moment from 'moment'



export default function Record({recordProp}){

	const {user} = useContext(UserContext)

	const {categoryName, categoryType, inputOn, amount, description} = recordProp	

	return(
		<Col xs={12} md={4}>
			<Card className="cardHighlight">
				<Card.Body>
					<Card.Title>
						<h2>{description}</h2>
					</Card.Title>
					<Card.Text>
						<h6>{categoryType} ({categoryName}) --------- PHP{amount}</h6>
					</Card.Text>
					<Card.Text>
						{moment(inputOn).format('MMMM')} {moment(inputOn).format('D')}, {moment(inputOn).format('YYYY')}  
					</Card.Text>
				</Card.Body>
			</Card>
		</Col>
	)
}

