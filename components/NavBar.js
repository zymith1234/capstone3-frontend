import {useContext, useEffect, useState} from 'react'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import Link from 'next/link'
import UserContext from '../UserContext'

export default function NavBar(){
	
	const {user} = useContext(UserContext)

	useEffect(()=>{},[user])

	return (
		<Navbar bg="light" expand="lg">
			<Link href="/">
				<a className="navbar-brand">Budget Tracking</a>
			</Link>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="mr-auto">
					{
						user.email ? 
						<>
							<Link href="/categories">
								<a className="nav-link" role="button">Categories</a>
							</Link>
							<Link href="/records">
								<a className="nav-link" role="button">Records</a>
							</Link>
							<Link href="/monthly-income">
								<a className="nav-link" role="button">Monthly Income</a>
							</Link>
							<Link href="/monthly-expense">
								<a className="nav-link" role="button">Monthly Expense</a>
							</Link>
							<Link href="/trend">
								<a className="nav-link" role="button">Balance Trend</a>
							</Link>
							<Link href="/category-breakdown">
								<a className="nav-link" role="button">Breakdown</a>
							</Link>
							<Link href="/logout">
								<a className="nav-link" role="button">Logout</a>
							</Link>
						</> 
						:
						<>
							<Link href="/login">
								<a className="nav-link" role="button">Login</a>
							</Link>
							<Link href="/register">
								<a className="nav-link" role="button">Register</a>
							</Link>
						</>
					}
				</Nav>
			</Navbar.Collapse>	
		</Navbar>
	)
}