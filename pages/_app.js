import {useState,useEffect} from 'react'
import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import {Container} from 'react-bootstrap'
import {UserProvider} from "../UserContext"
import NavBar from '../components/NavBar'
import AppHelper from '../app_helper'

function MyApp({ Component, pageProps }) {
	
	const [token, setToken] = useState("")
	const [user, setUser] = useState({
  		id: null,
    	email: null
	}) 
  	const unsetUser = () => {
  		localStorage.clear()
  		setUser({
      		id: null,
  			email: null
  		})
  	}
  	useEffect(()=>{
  			setToken(localStorage.getItem('token'))
  			if (token != ""){
		    	fetch(`${AppHelper.API_URL}/users/details`,{
		    		method: "GET",
		    		headers: {
		        		'Authorization': `Bearer ${token}`
		      		}
		    	})
		    	.then(res => res.json())
		    	.then(data => {
		      		if (data._id){
		        		setUser({
		          			id: data._id,
		          			email: data.email
		        		})
		      		} else {
		        		setUser({
		          			id: null,
		          			email: null
		        		})
		      		}
		    	})
  			}
	},[token])

	return (
		<UserProvider value={{user,setUser,unsetUser}}>
  			<NavBar/>
	  		<Container>
	  			<Component {...pageProps} />
	  		</Container>
  		</UserProvider>
  	)
}

export default MyApp
