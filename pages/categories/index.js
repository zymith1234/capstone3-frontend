import Button from 'react-bootstrap/Button'
import Link from 'next/link'
import {useEffect,useState,useContext} from 'react'
import Table from 'react-bootstrap/Table'
import AppHelper from '../../app_helper'
import UserContext from '../../UserContext'
import Category from '../../components/Category'

export default function categories(){

	const {user,setUser} = useContext(UserContext)

	const [categoriesData, setCategoriesData] = useState([])
	const [tokenData, setTokenData] = useState("")

	useEffect(()=>{
		setTokenData(localStorage.getItem('token'))
		console.log(tokenData)
		if (tokenData != ""){	
			fetch(`${AppHelper.API_URL}/users/details/categories`, {
				headers: {
					"Authorization": `Bearer ${tokenData}`
				}
			})
			.then(AppHelper.toJSON)
			.then(data=>{
				console.log(data)
				const categ = data.map(categData =>{
					return <Category categoryProp={categData}/>	
				})
				setCategoriesData(categ)
			})
		}
	},[tokenData])

	return (
		<>
			<h1>Categories</h1>
			<Button variant="success">
				<Link href="/categories/new">
					<a className="text-white" role="button">Add</a>
				</Link>
			</Button>
			<Table striped bordered hover>
				<thead>
					<tr>
						<th>Category</th>
						<th>Type</th>
					</tr>
				</thead>
				<tbody>
					{categoriesData}
				</tbody>
			</Table>

		</>
	)
}