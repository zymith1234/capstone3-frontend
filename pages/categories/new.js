import {useEffect,useState,useContext} from 'react'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import UserContext from '../../UserContext'
import AppHelper from '../../app_helper'
import { Input } from "reactstrap";
import Router from 'next/router'

export default function categories(){
	const {user,setUser} = useContext(UserContext)


	const [token, setToken] = useState("")
	const [ categoryName , setCategoryName ] = useState('')
	const [ categoryType , setCategoryType ] = useState('')

	const [ isActive , setIsActive ] = useState(false)

	useEffect(()=>{
		if (categoryName !== '' && categoryType !== ''){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
		setToken(localStorage.getItem("token"))
	},[token])

	function addCategory(e){
		e.preventDefault()

		fetch(`${AppHelper.API_URL}/users/new-category`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({
				categoryName: categoryName,
				categoryType: categoryType
			})
		})
		.then(AppHelper.toJSON)
		.then(data => {
			if(data === true){
				alert("Successfully added a category")
				Router.push('/categories')
			} else {
				alert("Failed to add a category")
			}
		})
	}

	return (
		<>
			<h1>New Category</h1>
			<Form onSubmit={e => addCategory(e)}>
				<Form.Group>
					<Form.Label>Category Name:</Form.Label>
					<Form.Control type="string" placeholder="Enter category name" value={categoryName} onChange={e => setCategoryName(e.target.value)} required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Category Type:</Form.Label>
					<div>
						<select name="categoryType" id="selectType" value={categoryType} onChange={(e)=>setCategoryType(e.target.value)}>
							<option value="expense">Expense</option>
							<option value="income">Income</option>
						</select>				
					</div>
				</Form.Group>
				<Button variant="primary" type="submit">Submit</Button>
			</Form>
		</>
	)
}