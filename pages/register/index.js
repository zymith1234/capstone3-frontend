import {useEffect,useState,useContext} from 'react'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import UserContext from '../../UserContext'
import Router from 'next/router'
import AppHelper from '../../app_helper'

export default function register(){

	const {user} = useContext(UserContext)

	const [ email , setEmail ] = useState("")
	const [ password1 , setPassword1 ] = useState("")
	const [ password2 , setPassword2 ] = useState("")
	const [ firstName, setFirstName] = useState("")
	const [lastName, setLastName] = useState("")
	const [ isActive , setIsActive ] = useState(false)


	useEffect(()=>{
		if((email !== '' && password1 !== '' && password2 !== '') && (password2 === password1)){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	},[email,password1,password2])

	function registerUser(e){
		e.preventDefault()
		setEmail('')
		setPassword1('')
		setPassword2('')
		setFirstName('')
		setLastName('')

		fetch(`${AppHelper.API_URL}/users/email-exists`, {
			method: 'POST', 
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({ 
				email: email
			})
		})
		.then(AppHelper.toJSON) 
		.then(data => { 
			if(data === false){
				fetch(`${AppHelper.API_URL}/users/`, {
					method: "POST",
					headers: {
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password1
					})
				})
				.then(AppHelper.toJSON)
				.then(data => {
					if(data === true){
						alert("Registration Successful")
						Router.push('/')
					} else {
						alert("Registration Failed")
					}
				})
			} else {
				alert("Email Already Exists.")
			}
		})
	}

	return(
			<Form onSubmit={(e)=> registerUser(e)}>
				<Form.Text className="text-muted">
						We'll never share your info with anyone else.
				</Form.Text>
				<Form.Group controlId="userFirstName">
					<Form.Label>Firstname:</Form.Label>
					<Form.Control type="string" placeholder="Firstname" 
					value={firstName} onChange={e => setFirstName(e.target.value)} required/>
				</Form.Group>
				<Form.Group controlId="userLastName">
					<Form.Label>Lastname:</Form.Label>
					<Form.Control type="string" placeholder="Lastname" 
					value={lastName} onChange={e => setLastName(e.target.value)} required/>
				</Form.Group>
				<Form.Group controlId="userEmail">
					<Form.Label>Email Address</Form.Label>
					<Form.Control type="email" placeholder="Enter Email" 
					value={email} onChange={e => setEmail(e.target.value)} required/>
				</Form.Group>
				<Form.Group controlId="password1">
					<Form.Label>Password:</Form.Label>
					<Form.Control type="password" placeholder="Password" 
					value={password1} onChange={e => setPassword1(e.target.value)} required/>
				</Form.Group>
				<Form.Group controlId="password2">
					<Form.Label>Confirm Password:</Form.Label>
					<Form.Control type="password" placeholder="Verify Password" value={password2} onChange={e => setPassword2(e.target.value)} required />
				</Form.Group>
				{

					isActive 
					? <Button variant="primary" type="submit">Submit</Button>
					: <Button variant="primary" disabled>Submit</Button>

				}
			</Form>
	)
}