import {Line} from 'react-chartjs-2';
import {useState, useEffect} from 'react'
import moment from 'moment'
import AppHelper from '../../app_helper'
import Calendar from 'react-calendar'
import DatePicker from 'react-datepicker'
import Head from "next/head";

export default function Trend(){

  const [months,setMonths] = useState([])
  const [monthlyBalance, setMonthlyBalance] = useState([])
  const [token, setToken] = useState("")
  const [dateFrom, setDateFrom] = useState(new Date())
  const [dateTo, setDateTo] = useState(new Date()) 
  const [allRecords, setAllRecords] = useState([])

  //save all records
  useEffect(()=>{
    setToken(localStorage.getItem("token"))
    let refMonths = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
    setMonths(refMonths)
    if (token != ""){
      fetch(`${AppHelper.API_URL}/users/details/records`, {
        headers: {
          "Authorization": `Bearer ${token}`
        }
      })
      .then(AppHelper.toJSON)
      .then(data=>{
        setAllRecords(data)
      })  
    }
  },[token])


  //save all months between the start date and end date when start date changes
  useEffect(()=>{
    let allMonthsInPeriod = []
    let startDate = moment(dateFrom, "YYYY-M-DD")
    let endDate = moment(dateTo,"YYYY-M-DD")
    while (startDate < endDate) {
      allMonthsInPeriod.push(startDate.format("MM-YYYY"));
      startDate = startDate.add(1, "month");
    };
    setMonths(allMonthsInPeriod);
  },[dateFrom])


  //save all months between the start date and end date when end date changes
   useEffect(()=>{
    let allMonthsInPeriod = []
    let startDate = moment(dateFrom, "YYYY-M-DD")
    let endDate = moment(dateTo,"YYYY-M-DD")
    while (startDate < endDate) {
      allMonthsInPeriod.push(startDate.format("MM-YYYY"));
      startDate = startDate.add(1, "month");
    };
    setMonths(allMonthsInPeriod);
  },[dateTo])
 

  //get the balance between the start date and end date
  useEffect(()=>{
    let amount = 0
    setMonthlyBalance(months.map(result=>{
      allRecords.forEach(element=>{
        let elementMonthYear = moment(element.inputOn).format('MM-YYYY')
        let elementInputOnDate = moment(element.inputOn).format("YYYY-M-DD")
        let dateFromMoment = moment(dateFrom).format("YYYY-M-DD") 
        let dateToMoment = moment(dateTo).format("YYYY-M-DD")
        let categoryTypeLowerCase = element.categoryType.toLowerCase()
        if( elementMonthYear == result && elementInputOnDate > dateFromMoment && elementInputOnDate < dateToMoment && categoryTypeLowerCase == "income"){
          console.log(true)    
          amount = amount + parseInt(element.amount)
            
        } else if( elementMonthYear == result && elementInputOnDate > dateFromMoment && elementInputOnDate < dateToMoment && categoryTypeLowerCase == "expense"){
          
          console.log(false)    
          amount = amount - parseInt(element.amount)
            
        }
      })
      return amount
    }))
  },[months])

   
   const data = {
    labels: months,
    datasets: [
      {
        label: "Balance",
        fill: false,
        lineTension: 0.1,
        backgroundColor: 'rgba(75,192,192,0.4)',
        borderColor: 'rgba(75,192,192,1)',
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBorderColor: 'rgba(75,192,192,1)',
        pointBackgroundColor: '#fff',
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: 'rgba(75,192,192,1)',
        pointHoverBorderColor: 'rgba(220,220,220,1)',
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: monthlyBalance
      }
    ]
  };

  return (
    <>
      <Head>
        <link
          rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/react-datepicker/2.14.1/react-datepicker.min.css"
        />
      </Head>
      <div>
        <h2>Balance Trend</h2>
      </div>
      <div>
        <h6>Start Date:</h6>
        <DatePicker
            valueName="selected"
            selected={dateFrom}
            onChange={(date) => setDateFrom(date)}
            dateFormat="MM/dd/yyyy"
            placeholderText="Select Date"
            name="DatePicker"
            defaultValue={null}
          />
      </div>
      <div>
        <h6>End Date:</h6>
        <DatePicker
            valueName="selected"
            selected={dateTo}
            onChange={(date) => setDateTo(date)}
            dateFormat="MM/dd/yyyy"
            placeholderText="Select Date"
            name="DatePicker"
            defaultValue={null}
          />
      </div>
      <div>
        <Line data={data} />
      </div>
    </>
  )
}