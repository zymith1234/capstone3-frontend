import {useState, useEffect} from 'react'
import {Bar} from 'react-chartjs-2'
import moment from 'moment'
import AppHelper from '../../app_helper'

export default function MonthlyIncome(){
	const [months,setMonths] = useState([])
	const [monthlyExpense, setMonthlyExpense] = useState([])
	const [token, setToken] = useState("")

	useEffect(()=>{
		setToken(localStorage.getItem("token"))
		let refMonths = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
		setMonths(refMonths)
		if (token != ""){
			fetch(`${AppHelper.API_URL}/users/details/records`, {
				headers: {
					"Authorization": `Bearer ${token}`
				}
			})
			.then(AppHelper.toJSON)
			.then(data=>{
				console.log(data)
				setMonthlyExpense(months.map(month=>{
					let amount = 0
					data.forEach(element=>{
						if(moment(element.inputOn).format('MMMM') === month && element.categoryType == "expense"){
							amount = amount + parseInt(element.amount)
						}
					})
					return amount
				}))
			})	
		}
	},[token])

	const data2 = {
		labels: months,
		datasets:[
			{
				label: 'Monthly Expense',
				backgroundColor: 'rgba(255, 99, 132, 0.2)',
				borderColor: 'rgba(255, 99, 132, 1)',
				borderWidth: 1,
				hoverBackgroundColor: 'rgba(255, 99, 132, 0.4)',
				hoverBorderColor: 'rgba(255, 99, 132, 1)',
				data: monthlyExpense
			}
		]
	}

	return(
		<>	
			<div>
				<h1>Monthly Expense</h1>
			</div>
			<div>
				<Bar data={data2}/>
			</div>
		</>
	)
}
