import {useState, useEffect} from 'react'
import {Pie} from "react-chartjs-2"
import {colorRandomizer} from '../../helpers/colorRandomizer'
import AppHelper from '../../app_helper'

export default function categoryBreakdown(){
	const [token, setToken] = useState("")
	const [categories,setCategories] = useState([])
	const [records,setRecords] = useState([])
	const [categoryNames, setCategoryNames] = useState([])
	const [totalAmount, setTotalAmount] = useState([])
	const [bgColors, setBgColors] = useState([])

	useEffect(()=>{
		setToken(localStorage.getItem("token"))
		if (token != ""){	
			fetch(`${AppHelper.API_URL}/users/details/categories`, {
				headers: {
					"Authorization": `Bearer ${token}`
				}
			})
			.then(AppHelper.toJSON)
			.then(data=>{
				setCategories(data)
			})
		}
	},[token])

	useEffect(()=>{
		if (token != ""){	
			fetch(`${AppHelper.API_URL}/users/details/records`, {
				headers: {
					"Authorization": `Bearer ${token}`
				}
			})
			.then(AppHelper.toJSON)
			.then(data=>{
				setRecords(data)
			})
		}
	},[categories])

	useEffect(()=>{
		setCategoryNames(categories.map(data=>{return data.categoryName}))
	},[records])

	useEffect(()=>{
		setTotalAmount(categories.map(data=>{
			let tempAmount = 0
			records.forEach(element=>{
				if(data.categoryName == element.categoryName && data.categoryType == element.categoryType){
					tempAmount = tempAmount + parseInt(element.amount)
				}
			})
			return tempAmount
		}))
	},[categoryNames])

	useEffect(()=>{
		setBgColors(categoryNames.map(()=>`#${colorRandomizer()}`))
	},[totalAmount])

	const dataPie = {
  		labels: categoryNames, 
  		datasets: [{
  			data: totalAmount,
  			backgroundColor: bgColors,
  			hoverBackgroundColor: bgColors
  		}]
  	}
	return (
		
		<>	
			<div>
				<h1>Category Breakdown</h1>
			</div>
			<div>
				<Pie data={dataPie} />
			</div>
		</>
	)
}