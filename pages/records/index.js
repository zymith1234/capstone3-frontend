import Button from 'react-bootstrap/Button'
import Link from 'next/link'
import {useEffect,useState,useContext} from 'react'
import Record from '../../components/Record'
import UserContext from '../../UserContext'
import AppHelper from '../../app_helper'
import Router from 'next/router'
import {Row,Col} from 'react-bootstrap'
import Card from 'react-bootstrap/Card'

export default function records(){

	const {user,setUser} = useContext(UserContext)

	const [records, setRecords] = useState([])
	const [token, setToken] = useState("")
	const [q, setQ] = useState("")
	const [results, setResults] = useState([])
	const [filteredRec, setFilteredRec] = useState([])
	const [recordTypeData, setRecordTypeData] = useState('All')


	useEffect(()=>{
		setToken(localStorage.getItem('token'))
		if (token != ""){
			fetch(`${AppHelper.API_URL}/users/details/records`, {
				headers: {
					"Authorization": `Bearer ${token}`
				}
			})
			.then(AppHelper.toJSON)
			.then(data=>{
				setRecords(data)
			})
		}		
	},[token])

	useEffect(()=>{
		if(q == ""){
			setResults(records.map(result=>{
				return <Record recordProp={result} />	
			}))
		}
	},[records])

	function search(records){
		return records.filter(result=> result.description.toLowerCase().indexOf(q.toLowerCase())>-1)
	}

	function searchType(records){
		return records.filter(result=> result.categoryType.toLowerCase().indexOf(recordTypeData.toLowerCase())>-1)
	}

	useEffect(()=>{
		if (recordTypeData === "All"){
			setFilteredRec(search(records))
		} else {
			setFilteredRec(search(searchType(records)))
		}
	},[recordTypeData])

	useEffect(()=>{
		if (recordTypeData === "All"){
			setFilteredRec(search(records))
		} else {
			setFilteredRec(search(searchType(records)))
		}
	},[q])

	useEffect(()=>{
		setResults(filteredRec.map(result=>{
			return <Record recordProp={result}/>	
		}))
	},[filteredRec])

			
	return (
		<>
			<h1>Records</h1>
			<Button variant="success">
				<Link href="/records/add">
					<a className="text-white" role="button">Add</a>
				</Link>
			</Button>
			<input type="text" value={q} onChange={(e)=> setQ(e.target.value)} />
			<select name="filterType" id="filterType" value={recordTypeData} onChange={(e)=>setRecordTypeData(e.target.value)}>
				<option value="All">All</option>
				<option value="Expense">Expense</option>
				<option value="Income">Income</option>
			</select>
			<Row>
				{results}
			</Row>
		</>
	)
}