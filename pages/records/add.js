import Button from 'react-bootstrap/Button'
import Link from 'next/link'
import {useEffect,useState,useContext} from 'react'
import Form from 'react-bootstrap/Form'
import UserContext from '../../UserContext'
import AppHelper from '../../app_helper'
import Router from 'next/router'


export default function addRecords(){

	const [categoryTypeData,setCategoryTypeData] = useState("expense")
	const [categoryNameData, setCategoryNameData] = useState("")
	const [amountData, setAmountData] = useState(0)
	const [descriptionData, setDescriptionData] = useState("")
	const [token, setToken] = useState("")
	const [allCategory, setAllCategory] = useState([])
	const [showCategory, setShowCategory] = useState([])
	const [nameFilteredByType, setNameFilteredByType] = useState([])

	useEffect(()=>{
		setToken(localStorage.getItem("token"))
		if (token != ""){	
			fetch(`${AppHelper.API_URL}/users/details/categories`, {
				headers: {
					"Authorization": `Bearer ${token}`
				}
			})
			.then(AppHelper.toJSON)
			.then(data=>{
				setAllCategory(data)
			})
		}
	},[token])

	function filterByType(allCategory){
		return allCategory.filter(result=> result.categoryType.toLowerCase().indexOf(categoryTypeData.toLowerCase())>-1)
	}

	useEffect(()=>{
		console.log(categoryTypeData.toLowerCase())
		setNameFilteredByType(filterByType(allCategory))
	},[allCategory])

	useEffect(()=>{
		console.log(categoryTypeData.toLowerCase())
		setNameFilteredByType(filterByType(allCategory))
	},[categoryTypeData])

	useEffect(()=>{
		console.log(nameFilteredByType)
		setShowCategory(nameFilteredByType.map(result=>{
			return <option value={result.categoryName}>{result.categoryName}</option>
		}))
	},[nameFilteredByType])

	function newRecord(e){
		
		e.preventDefault()
		if (token != ""){
			fetch(`${AppHelper.API_URL}/users/new-record`, {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${token}`
				},
				body: JSON.stringify({
					categoryName: categoryNameData,
					categoryType: categoryTypeData,
					description: descriptionData,
					amount: amountData
				})
			})
			.then(AppHelper.toJSON)
			.then(data => {
				if(data === true){
					alert("Successfully added a new record")
					Router.push('/records')
				} else {
					alert("Failed to add a new record")
				}
			})
		}
	}

	return (
		<>
			<h1>New Record</h1>
			<Form onSubmit={e=>newRecord(e)}>
				<Form.Group>
					<Form.Label>Category Type:</Form.Label>
					<div>
						<select name="categoryType" id="selectType" value={categoryTypeData} onChange={(e)=>setCategoryTypeData(e.target.value)}>
							<option value="expense">Expense</option>
							<option value="income">Income</option>
						</select>
					</div>
				</Form.Group>
				<Form.Group>
					<Form.Label>Category Name:</Form.Label>
					<div>
						<select name="categoryName" id="selectCategoryName" value={categoryNameData} onChange={(e)=>setCategoryNameData(e.target.value)}>
							{showCategory}
						</select>
					</div>				
				</Form.Group>
				<Form.Group>
					<Form.Label>Amount:</Form.Label>
					<Form.Control type="number" placeholder="Enter amount" value={amountData} onChange={e => setAmountData(e.target.value)} required/>				
				</Form.Group>
				<Form.Group>
					<Form.Label>Description:</Form.Label>
					<Form.Control type="string" placeholder="Enter description" value={descriptionData} onChange={e => setDescriptionData(e.target.value)} required/>				
				</Form.Group>  
				<Button variant="primary" type="submit">Submit</Button>
			</Form>
		</>
	)
}