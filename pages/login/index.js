import {useEffect, useState, useContext} from 'react'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import UserContext from '../../UserContext'
import Router from 'next/router'
import AppHelper from '../../app_helper'
import {GoogleLogin} from 'react-google-login'
import Swal from 'sweetalert2'

export default function login(){
	const {user,setUser} = useContext(UserContext)
	const [ email , setEmail ] = useState('')
	const [ password , setPassword ] = useState('')
	const [ isActive , setIsActive ] = useState(false)
	
	useEffect(()=>{
		if (email !== '' && password !== ''){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	},[email,password])

	const retrieveUserDetails = (accessToken) => {
		const options = {
			headers: {
				Authorization: `Bearer ${accessToken}`
			}
		}
		fetch(`${AppHelper.API_URL}/users/details`,options)
		.then(AppHelper.toJSON)
		.then(data => {
			setUser({
				id: data._id,
				email: data.email
			})
		})

	}

	function authenticate(e){
		e.preventDefault()
		fetch('http://localhost:4000/api/users/login',{
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res=>res.json())
		.then(data=>{
			if(data.accessToken){
				localStorage.setItem('token', data.accessToken)
				fetch('http://localhost:4000/api/users/details',{
					headers:{
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data => {
					setUser({
						id: data._id,
						email: data.email
					})
				})
				Router.push('/')
			} else {
				alert('Authentication Failed')
			}
		})	
		setEmail('')
		setPassword('')	
	}

	function authenticateGoogleToken(response){
		const payload = {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				tokenId: response.tokenId,
				accessToken: response.accessToken
			})
		}
		fetch(`${AppHelper.API_URL}/users/verify-google-id-token`,payload)
		.then(AppHelper.toJSON)
		.then(data=>{
			if(typeof data.accessToken !== "undefined"){
				localStorage.setItem('token', data.accessToken)
				Swal.fire({
					icon: 'success',
					title: "Successful Login"
				})
				console.log(data.accessToken)
				retrieveUserDetails(data.accessToken)
				Router.push('/')
			} else { 
				if (data.error == 'google-auth-error'){
					Swal.fire({
						icon: 'error',
						title: 'Google Authentication Failed'
					})
				} else if (data.error === 'login-type-error'){
					Swal.fire({
						icon: 'error',
						title: 'You may have registered through a different login procedure.'
					})
				}
			}
		})
	}
	return(
		<Form onSubmit={(e)=>authenticate(e)}>
			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control type="email" placeholder="Enter Your Email" value={email} onChange={e => setEmail(e.target.value)} required/>
			</Form.Group>
			<Form.Group controlId="password">
				<Form.Label>Password:</Form.Label>
				<Form.Control type="password" placeholder="Enter Your Password" value={password} onChange={e => setPassword(e.target.value)} required/>
			</Form.Group>
			{
				isActive ? <Button variant="primary" type="submit">Login</Button> : <Button variant="primary" disabled>Login</Button>
			}
			<GoogleLogin
				clientId="38077020784-v9t8p0lrj6qllvfadttb69qu9sic2ag4.apps.googleusercontent.com"
				buttonText="Login Using Google"
				cookiePolicy={'single_host_origin'}
				onSuccess={authenticateGoogleToken}
				onFailure={authenticateGoogleToken}
				className="w-100 text-center d-flex justify-content-center"
			/>		
		</Form>
	)
}